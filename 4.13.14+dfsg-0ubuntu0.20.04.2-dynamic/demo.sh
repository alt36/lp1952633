#!/bin/bash

service smbd start

# make sure smbd has started
sleep 2

/usr/bin/smbclient //127.0.0.1/test -U guest guest -c ls

echo Output of \"prexec.sh %i:\"
cat /tmp/preexec.log

service smbd stop
